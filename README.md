# House System

Fork of RealEstate by Michael Fitzmayer with more abilities.

**Note:** this branch now allow to have only one house per player. You should stick with master branch, if you want to allow players to have multiply houses.

### What's new?

- Interface to sell house(s)
- Interface to lock\unlock house(s)
- Interface to add\remove guests
- Interface to check which house you can buy at this moment
- Code cleanup, get rid of unnessesary cycles; Use 0.7 functions
- Rent system

### Requirements:

- [CoreScripts](https://gitlab.com/freedomland/corescripts)
- [Translation System](https://gitlab.com/freedomland/translation-system)
- [CommonFunctions](https://gitlab.com/freedomland/commonfunctions)
- [Timer Api Ex](https://gitlab.com/freedomland/timerapiex)
- [mpWidgets](https://gitlab.com/freedomland/mpwidgets)

### How to install

1. Put HouseSystem.lua to CoreScripts/scripts/custom
2. Put RealEstate directory to CoreScripts/data
3. Put content of directory translations in CoreScripts/scripts/translations
4. In config.lua add in config.customMenuIds table:

```lua
HSSell = 9004, HSMultsell = 9005, HSLock = 9006, HSGuestadd = 9007, HSGuestremove = 9008
```

Make sure numbers are uniq!
```

### License

Copyright 2018 Michael Fitzmayer

Copyright 2019 Freedom Land Team
