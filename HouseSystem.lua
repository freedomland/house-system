-- HouseSystem (c) 2019 Freedom Land Team

-- Original author is Michael Fitzmayer <mail@michael-fitzmayer.de>
-- This is fork by Freedom Land Team

inventoryHelper = require("inventoryHelper")
require("tr_cells")
mpWidgets = require("mpWidgets")
CommonFunctions = require("CommonFunctions")
timerApiEx = require("timerApiEx")

local pathData = tes3mp.GetDataPath() .. "/RealEstate/"
local basePrice = 500000
local abadonTime = 2678400
local baseRent = 500
local options = {}

local storage = jsonInterface.load("RealEstate/storage.json")
local rent = jsonInterface.load("RealEstate/rent.json")
local cells_sort = jsonInterface.load("RealEstate/cells_sort.json")
local rent_cells = jsonInterface.load("RealEstate/rent_cells.json")
local rentCell = "Tavern 'Lame Guar'"

timerApiEx.CreateLoopTimer("CheckRent", time.hours(4), -1, function() CheckRent() end)
timerApiEx.StartTimer("CheckRent")

timerApiEx.CreateLoopTimer("CheckHouse", time.hours(2), -1, function() CheckHouse() end)
timerApiEx.StartTimer("CheckHouse")

-- = Players cell = --

function GetPlayerCell(playerName)
	playerName = string.lower(playerName)

	for ind, cell in pairs(storage) do
		if cell.owner == playerName then
			return ind
		end
	end
	
	return nil
end

function GetPlayerRent(playerName)
	playerName = string.lower(playerName)

	for ind, cell in pairs(rent) do
		if rent.owner == playerName then
			return ind
		end
	end
	
	return nil
end

function CellListDialog(pid)
	local message = ""

	for houseid, price in pairs(cells_sort) do
		if storage[houseid] == nil or storage[houseid].owner == nil then
			message = message .. tr_cells(houseid) .. " : " .. tostring(price) .. "\n"
		end
	end
	
	tes3mp.ListBox(pid, -1, tr("List of available houses to buy:\nHouse : price"), message)
end

function CheckRealEstateCell(pid, cellCurrent)
	local admin = false
    local message = ""

	if Players[pid]:IsAdmin() or Players[pid]:IsOrdinator() then
		admin = true
	end
	
	if storage[cellCurrent] == nil then
		storage[cellCurrent] = {}
	end

	local cellOwner = storage[cellCurrent].owner
	local lockstate = storage[cellCurrent].isUnlocked
    local playerName = string.lower(Players[pid].accountName)

	if cellOwner ~= nil then
		if lockstate == true and playerName ~= cellOwner then
			message = color.White .. tr("You found house unlocked... If you think this is mistake ") .. color.Red .. tr("immediately ") .. color.White .. tr("tells admins about that.")
			
		elseif GuestListCheck(cellCurrent, playerName) then
			message = color.MediumSpringGreen .. tr("This house is belong to ") .. cellOwner .. tr(".\nBehave yourself.")
			storage[cellCurrent].lastVisit = os.time()
		
		elseif playerName ~= cellOwner and GuestListCheck(cellCurrent, playerName) == false and admin == false then
			message = color.Crimson .. tr("This house is belong to ") .. cellOwner .. "."
			WarpToPreviousPosition(pid)
		
		elseif playerName == cellOwner then
			message = color.MediumSpringGreen .. tr("Welcome home, ") .. playerName .. "."
			storage[cellCurrent].lastVisit = os.time()
			
		elseif playerName ~= cellOwner and admin == true then
			message = color.Crimson .. tr("This house is belong to ") .. cellOwner .. "."
		
		end
	
	else
		local housePrice = cells_sort[cellCurrent]
		
		if GetPlayerCell(playerName) == nil then
			message = message .. color.White .. tr("This house is on sale. You can buy it for ") .. housePrice .. tr(" Drakes. Type into chat ") .. color.Yellow .. "/house buy " .. color.White .. tr("to buy it.")
		else
			message = message .. color.Crimson .. "Этот дом продается, но у вас уже есть дом во владении. Продайте предыдущий дом, чтобы купить этот."
		end
    end
    
	tes3mp.MessageBox(pid, -1, message)
end

function CheckRentCell(pid, cellCurrent)
	local admin = false
    local message = ""

	if Players[pid]:IsAdmin() or Players[pid]:IsOrdinator() then
		admin = true
	end
	
	if rent[cellCurrent] == nil then
		rent[cellCurrent] = {}
	end

	local cellOwner = rent[cellCurrent].owner
    local playerName = string.lower(Players[pid].accountName)

	if cellOwner ~= nil then
		if playerName ~= cellOwner and admin == false then
			message = color.Crimson .. tr("This room is rented by ") .. cellOwner .. "."
			WarpToPreviousPosition(pid, rentCell)
		elseif playerName == cellOwner then
			message = color.MediumSpringGreen .. tr("Welcome home, ") .. playerName .. "."
		elseif playerName ~= cellOwner and admin == true then
			message = color.Crimson .. tr("This room is rented by ") .. cellOwner .. "."
		end
	else
		if GetPlayerRent(playerName) == nil then
			message = message .. color.White .. tr("This room is for rent. Type ") .. color.Yellow .. "/house rent" .. color.White .. tr(" in chat to get it.")
		else
			message = message .. color.Crimson .. "Эта комната сдается, но у вас уже есть арендованая комната или купленый дом."
		end
    end
    
	tes3mp.MessageBox(pid, -1, message)
end

function CellCheck(pid, cellCurrent)   
    if cells_sort[cellCurrent] ~= nil then
		CheckRealEstateCell(pid, cellCurrent)
	elseif rent_cells[cellCurrent] ~= nil then
		CheckRentCell(pid, cellCurrent)
	end
end

function CellBuy(pid)
    local cellCurrent = tes3mp.GetCell(pid)
    
    if cells_sort[cellCurrent] == nil then
		return
	end
    
    local message = ""
    local sendMessage = false
    local playerName = Players[pid].accountName

	if GetPlayerCell(playerName) ~= nil or GetPlayerRent(playerName) ~= nil then
		tes3mp.MessageBox(pid, -1, color.Crimson .. "У вас уже есть дом во владении.")
		return
	end

	if storage[cellCurrent].owner == nil then
		local housePrice = cells_sort[cellCurrent]
	
		if CommonFunctions.GetGold(pid) < housePrice then
			message = color.Crimson .. tr("To buy this house you need to have at least ") .. tostring(housePrice) .. tr(" Drakes.\n")
		else
			message = color.MediumSpringGreen .. tr("Welcome home, ") .. Players[pid].chat.accountName .. ".\n"
			mpWidgets.MessageBox(pid, "Этот дом теперь ваша собственность. Никто не сможет войти в дом, пока вы не дадите разрешение. Дом будет выставлен на торги автоматически, если вы не заходили в него в течении реального месяца. Вы, так же, можете продать дом за половину цены.")
			CellSetOwner(cellCurrent, pid)
			CommonFunctions.RemoveGold(pid, housePrice)
		end
    end

	tes3mp.MessageBox(pid, -1, message)
end

-- = Sell cell = --

function CellSell(pid, playerCell)
	local cellPrice = cells_sort[playerCell]/2
	local msg = string.format("Вы успешно продали дом '%s' за %s септимов.", tr_cells(playerCell), cellPrice)
	mpWidgets.MessageBox(pid, msg)
	CellRelease(playerCell)
	CommonFunctions.AddGold(pid, cellPrice)
end

function CellSellDialog(pid)
	local playerCell = GetPlayerCell(Players[pid].accountName)
	
	if playerCell == nil then
		tes3mp.MessageBox(pid, -1, tr("You do not own any house."))
		return
	end

	local cellPrice = cells_sort[playerCell]
	local cell = tr_cells(playerCell)
	
	local message = string.format(color.Red .. "Внимание!\n" .. color.White .. "Вы собираетесь продать '%s'. Обратите внимание, что дом будет продан за половину цены. Цена дома на момент покупки: %s септимов; после продажи вы получите: %s септимов. После нажатия ДА вы не сможете отменить сделку! Вы уверены, что хотите продать этот дом?", color.Yellow .. cell .. color.White, color.Yellow .. cellPrice .. color.White, color.Yellow .. cellPrice/2 .. color.White)
	
	local menuId = config.customMenuIds.HSSell
	
	mpWidgets.New(menuId, message)
	mpWidgets.AddButton(menuId, tr("Yes"), function() CellSell(pid, playerCell) end)
	mpWidgets.AddButton(menuId, tr("Cancel"), nil)
	mpWidgets.Show(pid, menuId, 1)
end

-- = Lock cell = --

function CellLockDialog(pid)
	local message = ""
    local playerCell = GetPlayerCell(Players[pid].accountName)
    
    if playerCell == nil then
		tes3mp.MessageBox(pid, -1, tr("You do not own any house."))
		return
	end
	
	local cell_t = tr_cells(playerCell)
	
	if storage[playerCell].isUnlocked == true then
		storage[playerCell].isUnlocked = false
		message = color.MediumSpringGreen .. cell_t .. tr(" has beel locked.\n")
	else
		storage[playerCell].isUnlocked = true
		message = color.DarkOrange .. cell_t .. tr(" was unlocked. Take care.\n")
	end

    tes3mp.MessageBox(pid, -1, message)
end

-- = Guest = --

function GuestListCheck(playerCell, guestName)
    local guestList = storage[playerCell].guestList

    if guestList == nil then
        return false
    end

    for index, item in pairs(guestList) do
        if item == guestName then
            return true
        end
    end

    return false
end

function GuestAddDialog(pid, guestName)
	local message = ""
    local playerCell = GetPlayerCell(Players[pid].accountName)
    
    guestName = string.gsub(guestName, "\"", "")
    guestName = string.lower(guestName)
    
    if playerCell == nil then
		tes3mp.MessageBox(pid, -1, tr("You do not own any house."))
		return
	end
	
	if GuestListCheck(playerCell, guestName) == true then
		message = color.DarkOrange .. guestName .. tr(" already in your guest list.\n")
	else	
		if storage[playerCell].guestList == nil then
			storage[playerCell].guestList = {}
		end

		table.insert(storage[playerCell].guestList, guestName)
		
		message = color.MediumSpringGreen .. guestName .. tr(" is now guest of ") .. tr_cells(playerCell) .. ".\n"
		
		jsonInterface.quicksave("RealEstate/storage.json", storage)
	end

    tes3mp.MessageBox(pid, -1, message)
end

function GuestRemoveDialog(pid, guestName)
	local message = ""
    local playerCell = GetPlayerCell(Players[pid].accountName)
    
    guestName = string.gsub(guestName, "\"", "")
    guestName = string.lower(guestName)
    
    if playerCell == nil then
		tes3mp.MessageBox(pid, -1, tr("You do not own any house."))
		return
	end
	
	local cell_t = tr_cells(playerCell)
	
	if GuestListCheck(playerCell, guestName) then
		for index, item in pairs(storage[playerCell].guestList) do
			if item == guestName then
				table.remove(storage[playerCell].guestList, index)
			end
		end
		
		message = color.MediumSpringGreen .. guestName .. tr(" was banished from ") .. cell_t .. ".\n"
		jsonInterface.quicksave("RealEstate/storage.json", storage)
	else
		message = color.DarkOrange .. guestName .. tr(" is not in guest list ") .. cell_t .. ".\n"
	end

    tes3mp.MessageBox(pid, -1, message)
end

function GuestListDialog(pid)
    local message = ""
    local sendMessage = false

	local playerCell = GetPlayerCell(Players[pid].accountName)

    if playerCell == nil then
        tes3mp.MessageBox(pid, -1, color.Crimson .. tr("You do not own any house."))
        return
    end
    
    local guestList = storage[playerCell].guestList
	
	if guestList ~= nil then
		for i, t in pairs(guestList) do
			message = message .. t[2] .. " (" .. t[1] .. ")\n"
		end
	end
		
	tes3mp.ListBox(pid, -1, tr("Guests in your houses"), message, tr("Close"))
end

-- = Rent = --

function CellRent(currentCell, days, pid)  
    local message = ""
    local cellOwner = rent[currentCell].owner

	if cellOwner == nil then
		local housePrice = baseRent * days

		if CommonFunctions.GetGold(pid) < housePrice then
			message = color.Crimson .. tr("To rent this room you need to have at least ") .. tostring(housePrice) .. tr(" Drakes.\n")
		else
			message = color.MediumSpringGreen .. tr("You rent this room for ") .. days .. tr(" days.\n")
			RentSetOwner(currentCell, days, pid)
			CommonFunctions.RemoveGold(pid, housePrice)
		end
    end

	tes3mp.MessageBox(pid, -1, message)
end

function CellRentDialog(pid)
	local currentCell = tes3mp.GetCell(pid)
	
	if rent_cells[currentCell] == nil then
		return
	end

	local message = color.White .. "Вы можете арендовать эту комнату, максимум на 7 дней. Аренда будет продолжаться, даже если вы оффлаин, помните об этом! Текущая цена аренды " .. color.Yellow .. baseRent .. color.White .. tr(" multiply to days you want to rent this room.")
	
	local menuId = config.customMenuIds.HSRent
	
	mpWidgets.New(menuId, message)
	
	for day = 1, 7 do
		local buttonName = day .. " " .. tr("day ") .. baseRent * day .. tr(" gold")
		mpWidgets.AddButton(menuId, buttonName, function() CellRent(currentCell, day, pid) end)
	end
	
	mpWidgets.AddButton(menuId, tr("Cancel"), nil)
	mpWidgets.Show(pid, menuId, 1)
end

function RentSetOwner(currentCell, days, pid)
	if rent[currentCell] == nil then
		rent[currentCell] = {}
	end
	
	rent[currentCell].owner = string.lower(Players[pid].accountName)
    rent[currentCell].rentTime = os.time()
    rent[currentCell].rentDays = days
    jsonInterface.quicksave("RealEstate/rent.json", rent)
end

-- = Utils = --

function CheckRent()
	if rent == nil then
		return
	end
	
	for roomId, cell in pairs(rent) do
		if rent[roomId].rentTime ~= nil then
			if os.time() - cell.rentTime > cell.rentDays * 86400 then
				if logicHandler.IsPlayerNameLoggedIn(cell.owner) == true then
					local player = logicHandler.GetPlayerByName(cell.owner)
					Players[player.pid]:Message("// #ff0000Аренда комнаты закончилась!\n")
					tes3mp.MessageBox(player.pid, -1, "#ff0000Аренда комнаты закончилась!")
				end
				
				rent[roomId] = {}
				jsonInterface.quicksave("RealEstate/rent.json", rent)
			end
		end
	end
end

function CheckHouse()
	if storage == nil then
		return
	end
	
	for houseId, t in pairs(storage) do
		if os.time() - t.lastVisit > abadonTime then
			tes3mp.SendMessage(pid, string.format("// #FF0000 %s был заброшен и выставлен на продажу\n", tr_cells(houseId)), true)
			storage[houseId] = {}
			jsonInterface.quicksave("RealEstate/storage.json", storage)
		end
	end
end

function CellRelease(cell)
    if storage[cell] == nil then
        storage[cell] = {}
    end
    storage[cell] = {}
    jsonInterface.quicksave("RealEstate/storage.json", storage)
end

function CellSetOwner(cell, pid)
    if storage[cell] == nil then
        storage[cell] = {}
    end

    storage[cell].owner = string.lower(Players[pid].accountName)
    storage[cell].isUnlocked = false
    jsonInterface.quicksave("RealEstate/storage.json", storage)
end

function WarpToPreviousPosition(pid, cell)
    local posx = tes3mp.GetPreviousCellPosX(pid)
    local posy = tes3mp.GetPreviousCellPosY(pid)
    local posz = tes3mp.GetPreviousCellPosZ(pid)

	if cell == nil then
		CommonFunctions.COC(pid, "", {posX = posx, posY = posy, posZ = posz})
	else
		CommonFunctions.COC(pid, cell, {posX = posx, posY = posy, posZ = posz})
	end
end

commandInterface.AddButton("houses", "Дома", "Справка HouseSystem", 0, "")

commandInterface.AddCommand("house", {
	args = "rent",
	description = "Арендовать комнату",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) CellRentDialog(pid) end)

commandInterface.AddCommand("house", {
	args = "buy",
	description = "Купить дом",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) CellBuy(pid) end)

commandInterface.AddCommand("house", {
	args = "sell",
	description = "Продать дом",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) CellSellDialog(pid) end)

commandInterface.AddCommand("house", {
	args = "lock",
	description = "Заблокировать или разблокировать дом",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) CellLockDialog(pid) end)

commandInterface.AddCommand("house", {
	args = "add",
	arguments = "<имя гостя (без кавычек)>",
	description = "Добавить гостей в дом",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) GuestAddDialog(pid, tableHelper.concatenateFromIndex(cmd, 3)) end)

commandInterface.AddCommand("house", {
	args = "remove",
	arguments = "<имя гостя (без кавычек)>",
	description = "Удалить гостей из дома",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) GuestRemoveDialog(pid, tableHelper.concatenateFromIndex(cmd, 3)) end)

commandInterface.AddCommand("house", {
	args = "guest",
	description = "Показать гостей добавленых в дом",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) GuestListDialog(pid) end)

commandInterface.AddCommand("house", {
	args = "list",
	description = "Показать не проданые дома и их цены",
	menuSection = "houses",
	stuffRankRequire = 0
}, function(pid, cmd) CellListDialog(pid) end)

connect("OnCellLoad", CellCheck)
